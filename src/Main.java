public class Main {

    private static final String[][] myBooks = {
            {"The Shadow of thr Torturer", "Gene Wolfe", "1 January 1981"},
            {"The Old Man and the Sea", "Ernest Hemingway", "1 September 1952"},
            {"Midnight's Children", "Salam Rushdie", "15 April 1981"}};

    private static final String sourcepath = "./data/books.csv";
    private static final String destpath = "./data/mybooks.csv";

    public static void main(String[] args) {
        createLibrary();
    }

    private static void createLibrary() {
        Library library = new Library(sourcepath, destpath);

        System.out.println("Processing...");
        library.loadBooks();

        for (String[] bookString : myBooks) {
            boolean success = Library.addBook(bookString);
            if (!success) {
                System.out.println("Out of memory, could not add book to manager: '" +
                        bookString[0] + "," +
                        bookString[1] + "," +
                        bookString[2]);
            }
        }
        library.writeBooks();
        System.out.println("\nList of books in database");
        library.printBooks();
    }

    }














//    private static void Main(String[] args) {
//        Library library = new Library();
//
//        library.loadBooksFromFile("data/books.csv");
//    }
//}
