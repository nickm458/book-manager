public class Book {

    private String title;
    private String author;
    private String date;

    public Book(String title, String author, String date)
    {
        this.title = title;
        this.author = author;
        this.date = date;
    }

    public String getTitle() {
        return this.title;
    }
    public String getAuthor() {
        return this.author;
    }
    public String getDate() {
        return this.date;
    }

    public String getFormattedRecord() {
        return this.title + "," + this.author + "," + this.date + "/n";
    }

}
