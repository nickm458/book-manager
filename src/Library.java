import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.Buffer;

public class Library {
    private final Book[] books = new Book[10];
    private int bookEnd = 0;

    String sourcePath;
    String destPath;

    public Library(String sourcePath, String destPath) {
        this.sourcePath = sourcePath;
        this.destPath = destPath;
    }

    public boolean addBook(String[] newBook) {
        if (newBook == null || bookEnd == books.length) {
            return false;
        }
        books[bookEnd++] = new Book(newBook[0], newBook[1], newBook[2]);
        return true;
    }

    public void printBooks() {
        for (Book book : this.books) {
            if (book!=null) {
                System.out.println(book.getTitle() + ", " +
                        book.getAuthor() + ", " +
                        book.getDate());
            }
        }
    }

    public void loadBooks() {
        try {
            FileReader fileReader = new FileReader(this.sourcePath);
            BufferedReader bufferedReader = new BufferedReader((fileReader));

            String nextLine = bufferedReader.readLine();
            if (nextLine!=null) {
                String[] bookString = nextLine.split(",");
                addBook(bookString);
            }
        }
    }
    catch (Exception e) {
        System.out.println(e.getMessage());
        System.out.println(e.getStackTrace());
        System.exit(-1);
    }

    public void writeBooks() {
        try {
            FileWriter fileWriter = new FileWriter(this.destPath);

            fileWriter.append("Book Title,Author,Date/n");

            for (Book book : this.books) {
                if(book!=null) {
                    fileWriter.append(book.getFormattedRecord());
                }
            }
            fileWriter.flush();
            fileWriter.close();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(e.getStackTrace());
            System.exit(-1);
        }
    }









//
//    /*
//     * Class attributes
//     */
//    private ArrayList<Book> books;
//
//    /*
//     * Constructor
//     */
//    public Library() {
//
//    }
//
//    /*
//     * Methods
//     */
//    public boolean addBook(Book book) {
//        return true;
//    }
//
//    public boolean loadBooksFromFile(String path) {
//        return true;
//        /*    try
//        {
//            String csvFile = "data/books.csv";
//
//            FileReader fileReader = new FileReader(csvFile);
//            BufferedReader bufferedReader = new BufferedReader(fileReader);
//
//            String nextLine = bufferedReader.readLine();
//
//            for (i=...) {
//            }
//        }
//        */
//    }
//
//    public boolean writeBooksToFile(String path) {
//        return true;
//    }


}
